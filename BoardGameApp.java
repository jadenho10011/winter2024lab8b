import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Tile build = Tile.CASTLE;
		Board playingField = new Board();
		int numCastles = 7;
		int turns = 0;
		
		System.out.println("Welcome to this Board Game!");
		
		while(numCastles > 0 && turns < 8){
			int row = 0;
			int col = 0;
			
			System.out.println("---------------------------");
			System.out.print(playingField);
			System.out.println("Number of Castles not on the field: " + numCastles);
			System.out.println("Number of turns: " + turns);
			System.out.println("Enter a number to determine which row(0-4) to place a token on: ");
			row = scanner.nextInt();
			System.out.println("Enter a number to determine which column(0-4) to place a token on: ");
			col = scanner.nextInt();
			int placedToken = playingField.placeToken(row, col);
			
			while(placedToken < 0){
				System.out.println("Please re-enter your values");
				row = scanner.nextInt();
				col = scanner.nextInt();
				placedToken = playingField.placeToken(row, col);
			}
			if(placedToken == 1){
				System.out.println("There's a wall in that spot");
				turns++;
			}
			else if(placedToken == 0){
				System.out.println("A Castle was successfully placed");
				turns++;
				numCastles--;
			}
			System.out.print(playingField);	
		}
		if(numCastles == 0){
			System.out.println("You beat the game!");
		}
		else{
			System.out.println("You lost the game!");
		}
	}
}