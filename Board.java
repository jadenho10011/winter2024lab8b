import java.util.Random;

public class Board {
    private Tile[][] grid;
	Random rng = new Random();

    public Board() {
        grid = new Tile[5][5];
		
        for (int i = 0; i < 5; i++) {
			int randIndex = rng.nextInt(grid[i].length);
            for (int j = 0; j < 5; j++) {
				if(j == randIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				} else{
                grid[i][j] = Tile.BLANK;
				}
            }
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                stringBuilder.append(grid[i][j].getName()).append(" ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public int placeToken(int row, int col) {
		int whatsThere = 0;
        if (row < 0 || row >= 5 || col < 0 || col >= 5) {
            whatsThere = -2;
        }
		else if(grid[row][col] == Tile.WALL || grid[row][col] == Tile.CASTLE){
			whatsThere = -1;
		}
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			whatsThere = 1;
		}
		else if(grid[row][col] == Tile.BLANK){
			grid[row][col] = Tile.CASTLE;
			whatsThere = 0;
		}
		return whatsThere;
    }
}